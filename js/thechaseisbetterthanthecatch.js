if ( document.readyState != "loading" ){
	init();
} else {
	document.addEventListener( "DOMContentLoaded", init );
}

let utilityTemplate = document.querySelector( "[data-template='utility']" );
let utilityTarget = document.getElementById( "utility-container" );
let prevSearch = "";

function init() {
	window.searchIndex = elasticlunr( function() {
		this.addField( "title" );
		this.addField( "description" );
		this.addField( "example" );
		this.addField( "taglist_string" );
		this.setRef( "hash" );
		this.saveDocument( false );
	} );

	fetch( "data.json" )
		.then(
			response => response.json()
		)
		.then(
			data => onDataLoaded( data )
		);
}

function onDataLoaded( data ) {
	if ( typeof data === "object" && typeof data.utilities === "object" ) {

		data.utilities = data.utilities.sort( function( a, b ) {
			var x = a.title; var y = b.title;
			return ( ( x < y ) ? -1 : ( ( x > y ) ? 1 : 0 ) );
		} );

		data.utilities.forEach( cloneElement );

		document.querySelector( "#searchbar" ).addEventListener( "change", triggerSearch );
		document.querySelector( "#searchbar" ).addEventListener( "keyup", triggerSearch );
	}
}

function triggerSearch( ev ) {
	let term = document.querySelector( "#searchbar" ).value;
	let elms = document.querySelectorAll( "div.utility" );

	if ( term.length < 1 ) {
		elms.forEach( function( elm ) {
			elm.classList.remove( "hide-from-search" );
		} );
	}

	if ( term !== prevSearch && term.length >= 3 ) {
		let results = window.searchIndex.search( term, {} );
		let hashes = [];

		results.forEach( function( res ) {
			hashes.push( res.ref );
		} );

		elms.forEach( function( elm ) {
			elm.classList.remove( "hide-from-search" );
		} );

		elms.forEach( function( elm ) {
			if ( hashes.indexOf( elm.getAttribute( "data-hash" ) ) === -1 ) {
				elm.classList.add( "hide-from-search" );
			}
		} );
	}

	prevSearch = term;
}

function cloneElement( elm ) {
	let utilityClone = utilityTemplate.cloneNode( true );
	utilityClone.removeAttribute( "data-template" );

	elm.hash = generateHash( elm.title );
	utilityClone.setAttribute( "data-hash", elm.hash );

	// Clone basic properties
	let props = utilityClone.querySelectorAll( "[data-property]" );
	props.forEach( function( propElm ) {
		let attrVal = propElm.getAttribute( "data-property" );
		propElm.removeAttribute( "data-property" );
		propElm.innerHTML = elm[ attrVal ];
	} );

	// Clone taglist
	let taglist = utilityClone.querySelector( "[data-taglist]" );
	let taglistNode = taglist.querySelector( ".tag" );
	taglist.removeAttribute( "data-taglist" );

	elm.taglist_string = "";
	elm.taglist.forEach( function( tag ) {
		let tlNode = taglistNode.cloneNode( true );
		tlNode.innerHTML = tag;
		taglist.appendChild( tlNode );

		elm.taglist_string += "#" + tag + " ";
	} );

	taglist.removeChild( taglistNode );

	// Clone linklist
	let linklist = utilityClone.querySelector( "[data-linklist]" );
	let linklistNode = linklist.querySelector( ".linklist--item" );
	linklist.removeAttribute( "data-linklist" );

	Object.entries( elm.linklist ).forEach( ( [ key, val ] ) => {
		let lNode = linklistNode.cloneNode( true );
		let lANode = lNode.querySelector( "a" );
		lANode.innerHTML = key;
		lANode.href = val;
		linklist.appendChild( lNode );
	});

	linklist.removeChild( linklistNode );

	utilityTarget.appendChild( utilityClone );

	window.searchIndex.addDoc( elm );
}

function generateHash( forString ) {
	let hash = 0;
	if ( forString.length === 0 ) {
		return 0;
	}

	for ( let i = 0 ; i < forString.length; i++ ) {
		let char = forString.charCodeAt( i );
		hash = ( ( hash << 5 ) - hash ) + char;
		hash = hash & hash;
	}

	return "HC" + Math.abs( hash );
}